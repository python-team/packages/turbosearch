#!/usr/bin/python3

import sys
sys.path.insert(0, '/usr/share/turbosearch')

from turbosearch import turbosearch

if __name__ == '__main__':
    turbosearch.run()
